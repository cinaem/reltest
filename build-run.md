---
layout: archive
title:
excerpt:
tags: build, run, release, ictacademy, post
permalink: /build-run/
image:
  feature: headerBuildnRun.png
---

# Build & Run

## 1. Vorbereitungen

Dies ist ein Typoblindtext. An ihm kann man sehen, ob alle Buchstaben da sind und wie sie aussehen. 

### 1.1 Oft genutzte Worte

Manchmal benutzt man Worte wie Hamburgefonts, Rafgenduks oder Handgloves, um Schriften zu testen. Manchmal Sätze, die alle Buchstaben des Alphabets enthalten - man nennt diese Sätze »Pangrams«. Sehr bekannt ist dieser: The quick brown fox jumps over the lazy old dog. 

### 1.2 Typoblindtexte / Aufzählung

Oft werden in Typoblindtexte auch fremdsprachige Satzteile eingebaut (AVAIL® and Wefox™ are testing aussi la Kerning), um die Wirkung in anderen Sprachen zu testen. In Lateinisch sieht zum Beispiel fast jede Schrift gut aus. Quod erat demonstrandum. Seit 1975 fehlen in den meisten Testtexten die Zahlen, weswegen nach TypoGb. 204 § ab dem Jahr 2034 Zahlen in 86 der Texte zur Pflicht werden. Nichteinhaltung wird mit bis zu 245 € oder 368 $ bestraft. Genauso wichtig in sind mittlerweile auch Âçcèñtë, die in neueren Schriften aber fast immer enthalten sind.

* A
* B
* C
* D
* E

Ein wichtiges aber schwierig zu integrierendes Feld sind OpenType-Funktionalitäten. Je nach Software und Voreinstellungen können eingebaute Kapitälchen, Kerning oder Ligaturen (sehr pfiffig) nicht richtig dargestellt werden. 

### 1.3 Kerning

Dies ist ein Typoblindtext. An ihm kann man sehen, ob alle Buchstaben da sind und wie sie aussehen. 

```
The quick brown fox jumps over the lazy old dog. 
```

AVAIL® and Wefox™ are testing aussi la Kerning:

```
The quick brown fox jumps over the lazy old dog. 
T h e quick brown fox jumps over the lazy old dog.
The q u i c k brown fox jumps over the lazy old dog. 
The quick b r o w n fox jumps over the lazy old dog. 
The quick brown f o x jumps over the lazy old dog. 
The quick brown fox j u m p s over the lazy old dog. 
The quick brown fox jumps o v e r the lazy old dog. 
The quick brown fox jumps over t h e lazy old dog. 
The quick brown fox jumps over the l a z y old dog. 
The quick brown fox jumps over the lazy o l d dog. 
The quick brown fox jumps over the lazy old d o g. 
```

### 1.4 Tabelle

Dies ist ein Typoblindtext. An ihm kann man sehen, ob alle Buchstaben da sind und wie sie aussehen. 

|Nr. |Step|
|:--|:------|
10  |The
11  |quick
12  |brown
13  |fox
14  |jumps
15  |over
16  |the
17  |lazy
18  |old
19  |dog
20  |.


### Das Ende

Nichteinhaltung wird mit bis zu CHF 250.- bestraft.