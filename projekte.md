---
layout: archive
title:
excerpt: 
tags: projekte, release, ictacademy, post
permalink: /projekte/
image:
  feature: headerProjekte.jpg
---

# Projekte

Überall angefangene Projekte, keines fertiggestellt. 

## Blindtext

Ich werde hier noch die Stellung halten, bis eines zum Ende kommt. Arbeiten Sie nicht zuviel!

## The Release

Die Homepage zum Release wurde von der ICT Academy erstellt.

* Jekyll Design erstellen
* Markdowns kreieren
* Blindtexte einfügen
* Skript auf Gitlab hochladen

## Wie geht's weiter?

* Besprechung mit den Kunden
* Korrekte Texte und Bilder abfüllen
* Webseite online stellen

## Danke

Meine Mission ist erfüllt. Ich werde hier noch die Stellung halten, bis die Homepage online geht. Ich wünsche Ihnen noch einen schönen Tag. 